import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BandeauComponent } from './bandeau/bandeau.component';
import { BarreRechercheComponent } from './recherche/barre-recherche/barre-recherche.component';
import { SearchResultComponent } from './recherche/search-result/search-result.component';
import { MapComponent } from './map/map.component';
import { OwlModule } from 'ngx-owl-carousel';
import { HttpClientModule } from '@angular/common/http';
import { ConnexionComponent } from './connexion/connexion.component';
import { RechercheComponent } from './recherche/recherche.component';
import { EventEmitterService } from './recherche/search-result/event-emitter.service';
import { FormulaireComponent } from './formulaire/formulaire.component'



@NgModule({
    declarations: [
        AppComponent,
        BandeauComponent,
        BarreRechercheComponent,
        SearchResultComponent,
        MapComponent,
        ConnexionComponent,
        RechercheComponent,
        FormulaireComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        OwlModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [EventEmitterService],
    bootstrap: [AppComponent]
})
export class AppModule { }
