import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SearchResultService } from '../recherche/search-result/search-result.service';

@Component({
  selector: 'app-formulaire',
  templateUrl: './formulaire.component.html',
  styleUrls: ['./formulaire.component.css']
})
export class FormulaireComponent implements OnInit {
  eqt: any;
  id: number;
  getById: string = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=res_equipements_2017&refine.equipementid=";

  constructor(private route: ActivatedRoute, private service: SearchResultService) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get("idEqt");
    this.service.get(this.getById + this.id).subscribe(data => {
      this.eqt = data;
    })
  }

}
