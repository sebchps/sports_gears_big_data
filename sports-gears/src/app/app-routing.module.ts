import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConnexionComponent } from './connexion/connexion.component';
import { RechercheComponent } from './recherche/recherche.component';
import { FormulaireComponent } from './formulaire/formulaire.component';

const routes: Routes = [
  { path: 'co', component: ConnexionComponent },
  { path: '', component: RechercheComponent},
  { path: 'complete/:idEqt', component: FormulaireComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
