import { Component, OnInit } from '@angular/core';
import 'jquery-nice-select';
import { EventEmitterService } from '../search-result/event-emitter.service';
declare var $: any;

@Component({
    selector: 'app-barre-recherche',
    templateUrl: './barre-recherche.component.html',
    styleUrls: ['./barre-recherche.component.css']
})
export class BarreRechercheComponent implements OnInit {
    q = "";

    constructor(private eventEmitterService: EventEmitterService) { }

    ngOnInit() {
        $(document).ready(function () {
            $('select').niceSelect();
        });
    }

    onKeyup(event) {
        this.eventEmitterService.searchService(this.q)
    }
}
