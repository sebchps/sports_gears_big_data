import { Component, OnInit } from '@angular/core';
import { SearchResultService } from './search-result.service';
import { BarreRechercheComponent } from '../barre-recherche/barre-recherche.component';
import { EventEmitterService } from './event-emitter.service';
import * as L from 'leaflet'
import { mapChildrenIntoArray } from '@angular/router/src/url_tree';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
    selector: 'app-search-result',
    templateUrl: './search-result.component.html',
    styleUrls: ['./search-result.component.css']    
})
export class SearchResultComponent implements OnInit {
    equipments: any
    facetGroupsList: any
    url: string = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=res_equipements_2017&rows=5"
    map: string = "https://www.google.com/maps/embed/v1/place?q=lat,long&key=AIzaSyCnKKTfNBxKWmF25tvLItvFpxuoSTYsXaY&zoom=14"
    urls = ["assets/img/hero/piste.png", "assets/img/hero/streetbasket.png", "assets/img/hero/nike.png", "assets/img/hero/sds.jpg", "assets/img/hero/swo.jpg", "assets/img/hero/tennis2.png"]

    constructor(private result: SearchResultService, private eventEmitterService: EventEmitterService, private sanitizer:DomSanitizer) { }

    ngOnInit() {
        this.get()
        if (this.eventEmitterService.subsVar==undefined) {    
            this.eventEmitterService.subsVar = this.eventEmitterService.    
            invokeFirstComponentFunction.subscribe((name:string) => {    
                this.search(name);    
            });    
        }    
    }
    
    search(param: string) {
        this.url = this.url.split("&q=")[0]
        if (param != "") {
            this.url += "&q=" + param
            console.log(this.url)
        }
        this.get()
    }
    
    getMap(lat: string, long: string) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(this.map.replace("lat",lat).replace("long",long))
    }
    
    get() {
        this.result.get(this.url).subscribe(res => {
            this.equipments = res;
            this.imgFunction()
        })
    }

    imgFunction() {
        console.log(this.equipments.records)
        this.equipments.records.forEach(element => {
            var randomImg = this.urls[Math.floor(Math.random() * this.urls.length)];
            Object.assign(element,{"img":randomImg})
        });
        
    }

}
